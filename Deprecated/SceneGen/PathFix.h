// PathFix.h: declaration
//
//////////////////////////////////////////////////////////////////////

#if !defined(PATHFIX_H)
#define PATHFIX_H

#include "CrossPlatform.h"

char *pathfix( const char *path );

#endif
